import { Component, OnInit } from '@angular/core';
import { Person } from './person';
import {BankService} from '../bank.service';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  model: Person;
  submitted: boolean;

  constructor(private bankService: BankService) { }

  ngOnInit() {
  	this.model = new Person();
  	this.submitted = false;
  }

  onSubmit(): void {
  	this.submitted = true;
    this.bankService.pushCustomerData(this.model).subscribe(
      (data) => console.log(data)
    );
  }

  reset(): void {
    this.model = new Person();
    this.submitted = false;
  }

}
