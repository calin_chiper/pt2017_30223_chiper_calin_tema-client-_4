
export class Person {
	personalIdentificationNumber: number;
    firstName: string;
	lastName: string;
	email: string;
}