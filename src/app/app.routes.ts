
import {AccountListComponent} from './account-list/account-list.component';
import {AccountFormComponent} from './account-form/account-form.component';
import {CustomerFormComponent} from './customer-form/customer-form.component';
import {DepositComponent} from './deposit/deposit.component';
import {WithdrawComponent} from './withdraw/withdraw.component';
import {RouterModule, Routes} from '@angular/router';

const APP_ROUTES = [
  {path: 'accounts', component: AccountListComponent},
  {path: 'account_form', component: AccountFormComponent},
  {path: 'customer_form', component: CustomerFormComponent},
  {path: 'deposit', component: DepositComponent},
  {path: 'withdraw', component: WithdrawComponent},
  {path: '', component: AccountListComponent}
];

export const APP_ROUTES_PROVIDER = RouterModule.forRoot(APP_ROUTES);
