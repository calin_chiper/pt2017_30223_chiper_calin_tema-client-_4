import { Component, OnInit } from '@angular/core';
import { BankService } from '../bank.service';
import { Person } from '../customer-form/person';
import { BankAccount } from "../account-form/bank-account";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {

   customers: Person[];
  selectedCustomer: Person;
  bankAccounts: BankAccount[];
  amount: Amount;
  selectedAccount: BankAccount;
  submitted: boolean;
  newBalance: number;
  invalidTransaction: boolean;

  constructor(private bankService: BankService) { }

  ngOnInit() {
	this.bankService.fetchAllCustomerData().subscribe((data) => this.customers = data);
  this.amount = new Amount();
	this.submitted = false;
  this.invalidTransaction = false;
  }

  onSubmit(value: number) {
    this.bankService.withdraw(this.selectedAccount, value).subscribe(
      (data) => {
        console.log(data);
        this.submitted = true;
        this.invalidTransaction = false;
      },
      (error) => {
        this.invalidTransaction = true;
        this.submitted = false;
      }
    );
  	
  }

  retrieveAccounts() {
  	this.bankService.fetchAccountData(this.selectedCustomer).subscribe((data) => this.bankAccounts = data);
  }

  reset() {
  	this.submitted = false;
    this.invalidTransaction = false;
  	this.selectedCustomer = null;
  	this.selectedAccount = null;
  }

}

class Amount {
	value: number;
}

