import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { AccountListComponent } from './account-list/account-list.component';
import { DepositComponent } from './deposit/deposit.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import {APP_ROUTES_PROVIDER} from './app.routes';
import {BankService} from './bank.service';

@NgModule({
  declarations: [
    AppComponent,
    CustomerFormComponent,
    AccountFormComponent,
    AccountListComponent,
    DepositComponent,
    WithdrawComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTES_PROVIDER
  ],
  providers: [BankService],
  bootstrap: [AppComponent]
})
export class AppModule { }
