import { Component, OnInit } from '@angular/core';
import { BankService } from '../bank.service';
import { Person } from '../customer-form/person';
import { BankAccount } from "../account-form/bank-account";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  customers: Person[];
  selectedCustomer: Person;
  bankAccounts: BankAccount[];
  amount: Amount;
  selectedAccount: BankAccount;
  submitted: boolean;
  newBalance: number;

  constructor(private bankService: BankService) { }

  ngOnInit() {
	this.bankService.fetchAllCustomerData().subscribe((data) => this.customers = data);
  this.amount = new Amount();
	this.submitted = false;
  }

  onSubmit(value: number) {
    this.bankService.deposit(this.selectedAccount, value).subscribe((data) => console.log(data));
  	this.submitted = true;
  }

  retrieveAccounts() {
  	this.bankService.fetchAccountData(this.selectedCustomer).subscribe((data) => this.bankAccounts = data);
  }

  reset() {
  	this.submitted = false;
  	this.selectedCustomer = null;
  	this.selectedAccount = null;
  }

}

class Amount {
	value: number;
}
