import { Component, OnInit } from '@angular/core';
import { BankService } from '../bank.service';
import { Person } from '../customer-form/person';
import { BankAccount } from "../account-form/bank-account";

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {
  customers: Person[];
  accounts: BankAccount[];
  selectedCustomer: Person;
  selectedAccount: BankAccount;

  constructor(private bankService: BankService) { }

  ngOnInit() {
  	this.bankService.fetchAllCustomerData().subscribe((data) => this.customers = data);
  }

  setSelectedCustomer(customer: Person) {
  	this.selectedCustomer = customer;
  	this.bankService.fetchAccountData(customer).subscribe((data) => this.accounts = data);
  }

  setSelectedAccount(bankAccount: BankAccount) {
  	this.selectedAccount = bankAccount;
  }

  deleteSelectedAccount(bankAccount: BankAccount) {
  	this.bankService.deleteAccountData(bankAccount).subscribe(
  		(data) => console.log(data)
  	);
  	this.bankService.fetchAccountData(bankAccount.person).subscribe((data) => this.accounts = data);
  	this.selectedCustomer = null;
  	this.selectedAccount = null;
  }

  deleteSelectedCustomer(customer: Person) {
  	this.bankService.deleteCustomerData(customer).subscribe(
  		(data) => console.log(data)
  	);
  	this.bankService.fetchAllCustomerData().subscribe((data) => this.customers = data);
  	this.selectedCustomer = null;
  	this.selectedAccount = null;
  }
}