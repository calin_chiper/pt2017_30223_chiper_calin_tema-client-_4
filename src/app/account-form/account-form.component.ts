import { Component, OnInit } from '@angular/core';
import { BankAccount } from './bank-account';
import { Person } from '../customer-form/person';
import {BankService } from '../bank.service';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit {
  model: BankAccount;
  submitted: boolean;
  selectFields: Person[];

  constructor(private bankService: BankService) { }

  ngOnInit() {
  	this.model = new BankAccount();
  	this.submitted = false;
    this.bankService.fetchAllCustomerData().subscribe(
        (data) => this.selectFields = data
    );
  }

  onSubmit(): void {
  	this.submitted = true;
    this.bankService.pushAccountData(this.model).subscribe(
      (data) => console.log(data)
    );
  }

  reset(): void {
  	this.submitted = false;
  	this.model = new BankAccount();
  }

}
