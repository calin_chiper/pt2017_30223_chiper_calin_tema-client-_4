import {Person} from '../customer-form/person';

export class BankAccount {
	iban: number;
	balance: number;
	type: string;
	person: Person;
	
}