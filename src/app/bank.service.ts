import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";
import {Person} from './customer-form/person';
import {BankAccount} from './account-form/bank-account';
import {Router} from "@angular/router";

@Injectable()
export class BankService {
	private headers = new Headers({'Content-Type': 'application/json'});

  	constructor(private http: Http) { }


  pushCustomerData(person: Person): Observable<Person> {
  	let url = "http://localhost:8080/customer_form";
  	return this.http.post(url, JSON.stringify(person), {headers: this.headers}).map(
  		(response) => response.json()
  	);
  }

  fetchAllCustomerData(): Observable<Person[]> {
  	let url = "http://localhost:8080/accounts";
  	return this.http.get(url, {headers: this.headers}).map(
  		(response) => response.json()
  	);
  }

  pushAccountData(bankAccount: BankAccount): Observable<BankAccount> {
    let url = "http://localhost:8080/account_form";
    return this.http.post(url, JSON.stringify(bankAccount), {headers: this.headers}).map(
       (response) => response.json()
    );
  }

  fetchAccountData(person: Person): Observable<BankAccount[]> {
    let url = "http://localhost:8080/accounts/" + person.personalIdentificationNumber;
    return this.http.get(url, {headers: this.headers}).map(
      (response) => response.json()
    );
  }

  deleteAccountData(bankAccount: BankAccount): Observable<BankAccount> {
    let url = "http://localhost:8080/accounts/" + bankAccount.person.personalIdentificationNumber + "/" + bankAccount.iban;
    return this.http.delete(url, {headers: this.headers}).map(
      (response) => response.json()
    );
  }

  deleteCustomerData(person: Person): Observable<Person> {
    let url = "http://localhost:8080/accounts/" + person.personalIdentificationNumber;
    return this.http.delete(url, {headers: this.headers}).map(
      (response) => response.json()
    );
  }

  deposit(bankAccount: BankAccount, amount: number):Observable<BankAccount> {
    let url = "http://localhost:8080/deposit/" + amount;
    return this.http.post(url, JSON.stringify(bankAccount), {headers: this.headers}).map(
      (response) => response.json()
    );
  }

  withdraw(bankAccount: BankAccount, amount: number):Observable<BankAccount> {
    let url = "http://localhost:8080/withdraw/" + amount;
    return this.http.post(url, JSON.stringify(bankAccount), {headers: this.headers}).map(
      (response) => response.json()
    );
  }


  
}
