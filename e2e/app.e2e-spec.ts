import { BankClientPage } from './app.po';

describe('bank-client App', () => {
  let page: BankClientPage;

  beforeEach(() => {
    page = new BankClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
